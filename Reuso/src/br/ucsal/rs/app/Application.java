package br.ucsal.rs.app;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import br.ucsal.rs.data.LocalDAO;
import br.ucsal.rs.data.UserDAO;

public class Application {
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		JPanel panel = new JPanel();
		JPanel panelBt = new JPanel();
		JLabel label = new JLabel();
		JButton cepButton = new JButton();
		JButton motivaButton = new JButton();
		
		UserDAO.loadUsers();
		LocalDAO.loadLocais();
		
		frame.setSize(480,200);
		frame.setLocation(100, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("App");
		
		panel.setLayout(new BorderLayout());
		
		label.setText("Selecione a aplicação que deseja utilizar");
		label.setSize(50, 100);
		panel.add(BorderLayout.NORTH, label);
		
		cepButton.setText("Consultar CEP");
		motivaButton.setText("Mensagem Motivacional");
		panelBt.setLayout(new FlowLayout());
		panelBt.add(cepButton);
		panelBt.add(motivaButton);
		panel.add(BorderLayout.SOUTH, panelBt);
		frame.add(panel);
		frame.setVisible(true);
		
		ConsultaCep actionCep = new ConsultaCep();
		cepButton.addActionListener(actionCep);
		
		Motivacional actionMotiva = new Motivacional();
		motivaButton.addActionListener(actionMotiva);
		

	}

}
