package br.ucsal.rs.app.frames;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ConsultaCepFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private Container container = getContentPane();
	private JLabel label = new JLabel("Digite o Logradouro");
	private JTextField logradouroField = new JTextField();
	private JButton searchBt = new JButton("Buscar");

	public ConsultaCepFrame() {
		setLayouManager();
		setPosicaoTamanho();
		addComponentsToContainer();
	}

	private void setLayouManager() {
		container.setLayout(null);

	}

	private void setPosicaoTamanho() {
		label.setBounds(120, 20, 350, 50);
		logradouroField.setBounds(210, 100, 150, 30);
		searchBt.setBounds(210, 180, 150, 50);
	}

	private void addComponentsToContainer() {
		container.add(label);
		container.add(logradouroField);
		container.add(searchBt);
	}

	public JTextField getLogradouroField() {
		return logradouroField;
	}

	public JButton getSearchBt() {
		return searchBt;
	}

	public JLabel getlabel() {
		return label;
	}

}
