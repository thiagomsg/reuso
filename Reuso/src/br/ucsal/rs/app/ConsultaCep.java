package br.ucsal.rs.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import br.ucsal.rs.app.frames.ConsultaCepFrame;
import br.ucsal.rs.auth.Autenticavel;
import br.ucsal.rs.auth.AuthenticationManager;
import br.ucsal.rs.data.LocalDAO;
import br.ucsal.rs.data.User;

public class ConsultaCep implements ActionListener, Autenticavel {
	
	private ConsultaCepFrame cepFrame = new ConsultaCepFrame();
	private LocalDAO localDAO = new LocalDAO();
	private AuthenticationManager am;
	
	{
		cepFrame.setTitle("Consultar CEP");
		cepFrame.setBounds(600, 200, 600, 400);
		cepFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	private void authenticate() {
		am = new AuthenticationManager();
		am.authenticate();
		am.getLoginFrame().getLoginbt().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String username = am.getLoginFrame().getUserField().getText();
				String password = new String(am.getLoginFrame().getPassField().getPassword());
				User user = am.getUserByLogin(username, password);
				doAfterAuthentication(user);

			}
		});
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		authenticate();
	}

	@Override
	public void doAfterAuthentication(User u) {
		am.getLoginFrame().dispose();
		if (u == null) {
			JOptionPane.showMessageDialog(null, "Usuário ou senha inválidos.", "ERRO", JOptionPane.ERROR_MESSAGE, null);
		} else {
			cepFrame.getlabel().setText("Bem-Vindo " + u.getUsername() + "! Por favor, digite o logradouro.");
			cepFrame.setVisible(true);
			cepFrame.getSearchBt().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					String logradouro = cepFrame.getLogradouroField().getText();
					String cep = localDAO.search(logradouro);
					if (cep.isEmpty()) {
						JOptionPane.showMessageDialog(cepFrame, "Logradouro não encontrado", "Erro",
								JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(cepFrame, "CEP: " + cep, "Consulta Realizada",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}

			});
		}

	}

}
