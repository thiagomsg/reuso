package br.ucsal.rs.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import br.ucsal.rs.app.frames.MotivacionalFrame;
import br.ucsal.rs.auth.Autenticavel;
import br.ucsal.rs.auth.AuthenticationManager;
import br.ucsal.rs.data.User;

public class Motivacional implements ActionListener, Autenticavel {

	private static MotivacionalFrame motiFrame = new MotivacionalFrame();
	private AuthenticationManager am = new AuthenticationManager();

	static {
		motiFrame.setBounds(600, 200, 600, 400);
		motiFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

	}

	private void authenticate() {
		am.authenticate();
		am.getLoginFrame().getLoginbt().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String username = am.getLoginFrame().getUserField().getText();
				String password = new String(am.getLoginFrame().getPassField().getPassword());
				User user = am.getUserByLogin(username, password);
				doAfterAuthentication(user);
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		authenticate();

	}

	@Override
	public void doAfterAuthentication(User u) {
		am.getLoginFrame().setVisible(false);
		if (u == null) {
			JOptionPane.showMessageDialog(null, "Usuário ou senha inválidos.", "ERRO", JOptionPane.ERROR_MESSAGE, null);
		} else {
			motiFrame.setTitle("Mensagem para " + u.getUsername());
			motiFrame.setVisible(true);
		}
	}
}
