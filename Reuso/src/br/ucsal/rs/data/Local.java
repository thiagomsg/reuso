package br.ucsal.rs.data;

public class Local {
	private String logradouro;
	private String cep;

	public Local(String logradouro, String cep) {
		super();
		this.logradouro = logradouro;
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
}
