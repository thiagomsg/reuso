package br.ucsal.rs.data;

import java.util.ArrayList;
import java.util.List;

public class UserDAO {
	private static List<User> users = new ArrayList<>();

	public static void loadUsers() {
		User u1 = new User("thiago", "123");
		User u2 = new User("joao", "123");
		User u3 = new User("maria", "123");
		User u4 = new User("ana", "123");
		List<User> usersList = List.of(u1, u2, u3, u4);
		
		users.addAll(usersList);
	}
	public User search(String username, String password) {
		return users.stream()
				.filter( u -> u.getUsername().equals(username) && u.getPassword().equals(password))
				.findFirst().orElse(null);
	}
}
