package br.ucsal.rs.data;

import java.util.ArrayList;
import java.util.List;

public class LocalDAO {
	private static List<Local> locais = new ArrayList<>();

	public static void loadLocais() {
		Local l1 = new Local("Rua A", "95843-080");
		Local l2 = new Local("Rua B", "93411-073");
		Local l3 = new Local("Rua C", "94834-099");
		Local l4 = new Local("Rua D", "97155-020");
		locais.add(l1);
		locais.add(l2);
		locais.add(l3);
		locais.add(l4);
	}
	
	public String search(String logradouro) {
				
		return 	locais.stream()
				.filter(l -> l.getLogradouro().equals(logradouro))
				.map(Local :: getCep)
				.findFirst().orElse("");
		

	}
}
