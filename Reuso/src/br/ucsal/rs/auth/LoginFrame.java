package br.ucsal.rs.auth;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginFrame extends JFrame {

	
	private static final long serialVersionUID = 1L;
	Container container = getContentPane();
	JLabel userLabel = new JLabel("Usuário");
	JLabel passLabel = new JLabel("Senha");
	JButton loginBt = new JButton("LOGIN");
	JTextField userField = new JTextField();
	JPasswordField passField = new JPasswordField();

	public LoginFrame() {
		setLayouManager();
		setPosicaoTamanho();
		addComponentsToContainer();
	}

	private void setLayouManager() {
		container.setLayout(null);

	}

	private void setPosicaoTamanho() {
		userLabel.setBounds(20, 50, 100, 30);
		passLabel.setBounds(20, 120, 100, 30);
		userField.setBounds(80, 50, 100, 30);
		passField.setBounds(80, 120, 100, 30);
		loginBt.setBounds(60, 200, 120, 45);
	}

	private void addComponentsToContainer() {
		container.add(userLabel);
		container.add(passLabel);
		container.add(userField);
		container.add(passField);
		container.add(loginBt);

	}

	public JButton getLoginbt() {
		return loginBt;
	}

	public JTextField getUserField() {
		return userField;
	}

	public JPasswordField getPassField() {
		return passField;
	}

}
