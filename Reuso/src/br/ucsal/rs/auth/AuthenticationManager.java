package br.ucsal.rs.auth;

import javax.swing.WindowConstants;

import br.ucsal.rs.data.User;
import br.ucsal.rs.data.UserDAO;

public class AuthenticationManager{

	private UserDAO userDAO = new UserDAO();
	private LoginFrame loginFrame = new LoginFrame();


	public void authenticate() {
		loginFrame.setTitle("Login");
		loginFrame.setBounds(400, 200, 235, 360);
		loginFrame.setResizable(false);
		loginFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		loginFrame.setVisible(true);

		
	}

	public User getUserByLogin(String username, String password) {
		return userDAO.search(username, password);
	}
	
	public LoginFrame getLoginFrame() {
		return loginFrame;
	}

}
