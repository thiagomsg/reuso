package br.ucsal.rs.auth;


import br.ucsal.rs.data.User;

@FunctionalInterface
public interface Autenticavel {

	void doAfterAuthentication(User u);
		
}
